<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>

  <section class="py-3">
    <h1 id="title" class="text-center">Peticiones</h1>
  </section>

  <section class="d-flex justify-content-center align-items-center my-4 ">
    <div class="container-sm-md-lg">
      <form action="insert.php" method="post">
        <table>
          <div class="mb-3">
            <label class="form-label"><i class='bi bi-person-fill'></i>  Full Name</label>
            <input type="text" class="form-control" name="name" placeholder="Pepito Perez" style="width:500px">
          </div>
          <div class="mb-3">
            <label class="form-label"><i class="bi bi-envelope-at-fill"></i>  Email</label>
            <input type="email" class="form-control" name="email" placeholder="name@example.com" style="width:500px">
          </div>
          <div class="mb-3">
            <label class="form-label"><i class="bi bi-geo-alt-fill"></i>  City / Location</label>
            <input type="text" class="form-control" name="city" placeholder="Bogota" style="width:500px">
          </div>
          <div class="mb-3">
            <label class="form-label"><i class="bi bi-body-text"></i>  Petition</label>
            <input type="text" class="form-control" name="petition" placeholder="Bla Bla Bla" style="width:500px">
          </div>
          <div class="d-flex justify-content-center my-5">
              <input class="btn btn-outline-dark btn-lg mx-4" type="submit" name="bsubmit" value="Save">
              <input class="btn btn-outline-dark btn-lg mx-4" type="reset" name="breset" value="Reset">
          </div>
        </table>
      </form>
    </div>
  </section>
  

  <?php include 'dbcon.php' ?>
  <?php
    $selectquery = "SELECT * FROM client";
    $selres = mysqli_query($con1,$selectquery);
  ?>
  <section class="d-flex justify-content-center align-items-center my-4">
    <div class="container my-3">
      <div class="row d-flex justify-content-center align-items-center bg-dark text-light rounded-pill">
        <div class="col-md-1 col-lg-1">ID</div>
        <div class="col-md-2 col-lg-2">Name</div>
        <div class="col-md-2 col-lg-2">Email</div>
        <div class="col-md-2 col-lg-2">City</div>
        <div class="col-md-2 col-lg-2">Petition</div>
      </div>
    </div>
  </section>
  
  <section class="d-flex justify-content-center align-items-center">
    <table>
      <?php
        while ( $row = mysqli_fetch_array($selres)) {
          echo "<tr>";
          echo "<td class='text-left pr-3'>" . $row['id'] . "</td>"; 
          echo "<td class='px-4'>" . $row['name'] . "</td>"; 
          echo "<td class='px-3'>" . $row['email'] . "</td>"; 
          echo "<td class='px-3'>" . $row['city'] . "</td>"; 
          echo "<td class='px-3'>" . $row['petition'] . "</td>"; 
          echo "</tr>";
        }
      ?>
    </table>
  </section>
  

  <?php
    if(isset($_GET['success']) && $_GET['success'] == 'true') {
        echo "Peticion registrada exitosamente.";
    } elseif(isset($_GET['error']) && $_GET['error'] == 'true') {
        echo "Usuario ya había hecho una petición.";
    }
  ?>

</body>
</html>